from selenium import webdriver
from time import sleep

def url_who():
    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
    
    driver.find_element_by_xpath("/html/body/nav/div/div[2]/ul/li[3]/a").click()
    sleep(30)
    
    driver.find_element_by_xpath("/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div").click()
    driver.find_element_by_xpath("/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[17]/a/span[2]").click()    
    sleep(30)
    
    url_table = driver.find_element_by_id("cmd_export_data").get_attribute('href')
    #driver.find_element_by_id("cmd_export_data").click()
    
    driver.close()
    driver.quit()
#    url = 'https://ec.europa.eu/eurostat/web/main/home'
#    driver = webdriver.Firefox()
#    driver.maximize_window()
#    driver.get(url)
#    
#    driver.find_element_by_xpath("/html/body/div[2]/header/div[2]/section/div/div[2]/div/div[2]/div/ul/li[2]/a/span").click()
#    driver.find_element_by_xpath("/html/body/div[2]/section/div/div/div[2]/div/div[2]/section/div/div[2]/div/div/form/div/div/div[2]/div[3]/a/table/tbody/tr/td[6]/span").click()
#    driver.find_element_by_xpath("/html/body/div[2]/section/div/div/div[2]/div/div[2]/section/div/div[2]/div/div/form/div/div/div[2]/div[3]/div/div[4]/div/div[3]/div/a/table/tbody/tr/td[8]/span").click
#    
#    /html/body/div[2]/section/div/div/div[2]/div/div[2]/section/div/div[2]/div/div/form/div/div/div/div[1]/div/a/table/tbody/tr/td[5]/span
#    sleep(30)
#    
#    driver.find_element_by_xpath("/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div").click()
#    driver.find_element_by_xpath("/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[17]/a/span[2]").click()    
#    sleep(30)
#    
#    url_table = driver.find_element_by_id("cmd_export_data").get_attribute('href')
#    #driver.find_element_by_id("cmd_export_data").click()
#    
#    driver.close()
#    driver.quit()

    
    return url_table