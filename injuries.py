from download import download_data
from pathlib import Path
import country_converter as coco
import re
import pandas as pd 

from get_url import url_who
from injuries_calculation import non_fatal_calculation
from injuries_calculation import fatal_calculation
from injuries_calculation import who_calculation


storage_root = Path(".").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"

'''
URL of tables
'''

src_url = url_who()
src_csv = Path("WHO_ILO_BOD_LWH_2021_05.csv")

src_url2 = "https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=data/hsw_n2_02.tsv.gz"
src_csv2 = Path("hsw_n2_02.tsv")

src_url3 = "https://www.ilo.org/ilostat-files/WEB_bulk_download/indicator/INJ_FATL_ECO_NB_A.csv.gz"
src_csv3 = Path("INJ_FATL_ECO_NB_A.csv")

#src_url3 = "https://www.ilo.org/ilostat-files/WEB_bulk_download/indicator/INJ_NFTL_ECO_NB_A.csv.gz"
#src_csv3 = Path("INJ_NFTL_ECO_NB_A.csv")


'''
Download compressed files, save them in "download" folder
uncompressed files and save them in "data" folder
'''
download_data(src_url,src_csv,src_url2,src_csv2,src_url3,src_csv3,data_path)


'''
Calculation of  injuries
'''

injuries_fatal = fatal_calculation(data_path,src_csv2)
injuries_fatal = injuries_fatal.replace({':':''}, regex=True)
injuries_fatal[injuries_fatal.columns[3:]] = injuries_fatal[injuries_fatal.columns[3:]].replace({'[a-zA-Z]':''},regex=True)
injuries_fatal= injuries_fatal[injuries_fatal['nace_r2'].map(len) < 2]

injuries_fatal.columns = injuries_fatal.columns.str.strip()

for i in range(3,15):
    injuries_fatal[injuries_fatal.columns[i]]=pd.to_numeric(injuries_fatal[injuries_fatal.columns[i]],errors = 'coerce')
    injuries_fatal[injuries_fatal.columns[i]] = injuries_fatal[injuries_fatal.columns[i]].fillna(0)

injuries_fatal.loc[(injuries_fatal['geo\\time']=='UK'),['geo\\time']]='GB'
injuries_fatal.loc[(injuries_fatal['geo\\time']=='EL'),['geo\\time']]='GR'

injuries_fatal = injuries_fatal[~injuries_fatal['geo\\time'].str.contains('EU15|EU28|EU27_2007|EU27_2020',regex=True)]
geo_code = list(injuries_fatal['geo\\time'])

cc_all = coco.CountryConverter(include_obsolete=True)
injuries_fatal.insert(3, 'ISO3', cc_all.convert(names = geo_code, to='ISO3'))

injuries_fatal_aggregate=injuries_fatal.groupby(['unit','ISO3'],axis=0).sum()


who = who_calculation(data_path,src_csv)

who = who[who['age'].str.contains('≥15 years',regex=True)]
who = who[who['sex'].str.contains('Male|Female',regex=True)]
who = who[who['measure'].str.contains('Deaths',regex=True)]
who=who.drop(columns=['Population-attributable fraction (%)','Rate (per 100,000 of population)'])
who = who[who['year'] >1994]

country_code = list(who['country_name_who'])

who.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
who.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
who = who[~who['EXIO3'].str.contains('not found',regex=True)]
who=who.drop(columns=['risk_factor','age'])


who_aggregate=who.copy()
#who_aggregate=who_aggregate.groupby(['year','EXIO3','measure','estimate','sex'],axis=0).sum()
who_aggregate=who_aggregate.groupby(['year','ISO3','measure','estimate','sex'],axis=0).sum()



#injuries_split= pd.DataFrame(columns = ['ISO3','EXIOBASE sector','sex','estimate','year','death'])
#
#for code in injuries_fatal['geo\\time'].unique():
#    if code != 'not found':
#        print(code)
#        for a in injuries_fatal['nace_r2'].unique():
#            for b in who['sex'].unique():
#                for c in who['estimate'].unique():
#                    for year in range(2008,2020):
#                        injuries_split=injuries_split.append(pd.Series([code,a,b,c,year,0],index=[i for i in injuries_split.columns]),ignore_index=True)
#         
#for i in range(4,6):
#    injuries_split[injuries_split.columns[i]]=pd.to_numeric(injuries_split[injuries_split.columns[i]],errors = 'coerce')
#               
#for code in injuries_fatal['geo\\time'].unique():
#    if code in who['EXIO3'].unique():
#
#        for a in injuries_fatal['nace_r2'].unique():
#            for b in who['sex'].unique():
#                for c in who['estimate'].unique():
#                    for year in range(2008,2020):
#                        if (year==2010 or year==2016):
#                            print(year)
#                            print(code,a,b,c)
#                            injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(2010, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['geo\\time']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
#    
#
#for code in injuries_fatal['geo\\time'].unique():
#    print(code)
#    if code in who['EXIO3'].unique():
#        for a in injuries_fatal['nace_r2'].unique():
#            if a == 'A':
#                for b in who['sex'].unique():
#                    for c in who['estimate'].unique():
#                        for year in range(2008,2020):
#                            if (year<=2009):
#                                print(year)
#                                print(code,a,b,c)
#                                if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))):
#                                    print(float(who_aggregate.loc[who_aggregate.index==(2010, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)))
#                                    unknown_who = round(float(who_aggregate.loc[who_aggregate.index==(2010, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))))
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=round(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['geo\\time']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
#                                    
#                                else:
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=0
#                        for year in range(2008,2020):
#                            if (year>2010  & year<2016):
#                                print(year)
#                                print(code,a,b,c)
#                                if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))):
#                                    print(float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)))
#                                    unknown_who = round(float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))))
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=round(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['geo\\time']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
#                                    
#                                else:
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=0
#                        for year in range(2008,2020):
#                            if (year>2016):
#                                print(year)
#                                print(code,a,b,c)
#                                if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))):
#                                    print(float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)))
#                                    unknown_who = round(float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))))
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=round(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['geo\\time']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
#                                    
#                                else:
#                                    injuries_split.loc[(injuries_split['ISO3']==code)&(injuries_split['EXIOBASE sector']==a)&(injuries_split['sex']==b)&(injuries_split['estimate']==c)&(injuries_split['year']==year),'death']=0
#

workforce = pd.read_csv("aux/table_workforce_by_ISO3.csv")
workforce = workforce[~workforce['classif1'].str.contains('SECTOR|AGGREGATE',regex=True)]
workforce = workforce[~workforce['sex'].str.contains('SEX_T',regex=True)]
workforce=workforce.replace(to_replace='SEX_F',value='Female')
workforce=workforce.replace(to_replace='SEX_M',value='Male')



injuries_split_ROW= pd.DataFrame(columns = ['ISO3','EXIOBASE sector','sex','estimate','year','death'])

for code in who['ISO3'].unique():
    if code != 'not found':
        print(code)
        for a in injuries_fatal['nace_r2'].unique():
            for b in who['sex'].unique():
                for c in who['estimate'].unique():
                    for year in range(2008,2020):
                        injuries_split_ROW=injuries_split_ROW.append(pd.Series([code,a,b,c,year,0],index=[i for i in injuries_split_ROW.columns]),ignore_index=True)



for code in who['ISO3'].unique():
    print(code)
    if  code  not in injuries_fatal['ISO3'].unique() :
        for a in injuries_split_ROW['EXIOBASE sector'].unique():
            for b in injuries_split_ROW['sex'].unique():
                for c in who['estimate'].unique():
                    for year in range(2008,2020):
                        if (year==2010 or year==2016):
                            if float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)) != 0.0:                                        
                                if str('ECO_DETAILS_'+str(a)) in workforce.classif1.unique() : 
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))/float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)))
                                if ((a == 'D') or (a == 'E')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))/float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)))
                                if ((a == 'H') or (a == 'J')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))/float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)))
                                if ((a == 'L') or (a == 'M')or (a == 'N')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*(1/3)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))/float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)))
                                if ((a == 'R') or (a == 'S')or (a == 'T')or (a == 'U')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*(1/4)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))/float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)))
                            else :
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0
                        
    else:
        for a in injuries_split_ROW['EXIOBASE sector'].unique():
            for b in injuries_split_ROW['sex'].unique():
                for c in who['estimate'].unique():
                    for year in range(2008,2020):
                        if (year==2010 or year==2016):
                            if float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)) != 0.0:
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
                            else :
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0
            
        
for code in who['ISO3'].unique(): #14:06 29.04
#for code in country :
    print(code)
    if  code  in injuries_fatal['ISO3'].unique():
        for a in injuries_split_ROW['EXIOBASE sector'].unique():
            for b in injuries_split_ROW['sex'].unique():
                for c in who['estimate'].unique():
                    for year in range(2008,2020):
                        if (year<=2009):
                            if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)) != 0.0):
                                unknown_who = round(float(who_aggregate.loc[who_aggregate.index==(2010, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))))
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
                                
                            else:
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0
                    

                        if (year>2010  & year<2016):
                            if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)) != 0.0):
                                unknown_who = round(float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))))
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
                                
                            else:
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0

                        if (year>2016):
                            if (float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)) !=0 and float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)) != 0.0):
                                unknown_who = round(float(who_aggregate.loc[who_aggregate.index==(2016, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2016)].to_string(index=False,header=False)))))
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
                                
                            else:
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0

                            
    else:
        for a in injuries_split_ROW['EXIOBASE sector'].unique():
            for b in injuries_split_ROW['sex'].unique():
                for c in who['estimate'].unique():
                    for year in range(2008,2020):
                        if (year<=2009):
                            if float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)) != 0.0:                                        

                                if str('ECO_DETAILS_'+str(a)) in workforce.classif1.unique() : 
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False))*((float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))-float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False)))/(float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))))))                            
                                if ((a == 'D') or (a == 'E')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False))*((0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))-0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False)))/(0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))))))                            
                                if ((a == 'H') or (a == 'J')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False))*((0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))-0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False)))/(0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))))))                            
                                if ((a == 'L') or (a == 'M')or (a == 'N')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False))*(((1/3)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))-(1/3)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False)))/((1/3)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))))))                            
                                if ((a == 'R') or (a == 'S')or (a == 'T') or (a == 'U')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False))*(((1/4)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))-(1/4)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False)))/((1/4)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))))))                            

                            else :
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0
                    
                        if (year>2010  & year<2016):
                            if float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)) != 0.0:                                        

                                if str('ECO_DETAILS_'+str(a)) in workforce.classif1.unique() : 
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))*((float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))-float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False)))/(float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))))))
                                if ((a == 'D') or (a == 'E')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))*((0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))-0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False)))/(0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))))))                            
                                if ((a == 'H') or (a == 'J')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))*((0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))-0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False)))/(0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))))))                            
                                if ((a == 'L') or (a == 'M')or (a == 'N')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))*(((1/3)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))-(1/3)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False)))/((1/3)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))))))                            
                                if ((a == 'R') or (a == 'S')or (a == 'T') or (a == 'U')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))*(((1/4)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))-(1/4)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False)))/((1/4)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))))))                            
                           

                            else :
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0
                        
                        if (year>2016):
                            if float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)) != 0.0:                                        

                                if str('ECO_DETAILS_'+str(a)) in workforce.classif1.unique() : 
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))*((float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))-float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False)))/(float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))))))
                                if ((a == 'D') or (a == 'E')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))*((0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))-0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False)))/(0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_DE'),'obs_value'].to_string(index=False,header=False))))))                            
                                if ((a == 'H') or (a == 'J')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))*((0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))-0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False)))/(0.5*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_HJ'),'obs_value'].to_string(index=False,header=False))))))                            
                                if ((a == 'L') or (a == 'M')or (a == 'N')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))*(((1/3)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))-(1/3)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False)))/((1/3)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_LMN'),'obs_value'].to_string(index=False,header=False))))))                            
                                if ((a == 'R') or (a == 'S')or (a == 'T') or (a == 'U')):
                                    injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2016),'death'].to_string(index=False,header=False))*(((1/4)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))-(1/4)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False)))/((1/4)*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2016)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_RSTU'),'obs_value'].to_string(index=False,header=False))))))                            
           

                            else :
                                injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=0

injuries_split_ROW.to_csv("injuries_per_ISO3.csv",index=False)

ISO_code = list(injuries_split_ROW['ISO3'])
injuries_split_ROW.insert(1, 'EXIO3', cc_all.convert(names = ISO_code, to='EXIO3'))
injuries_split_EXIO3=injuries_split_ROW.copy()

injuries_split_EXIO3_aggregate=injuries_split_EXIO3.groupby(['EXIO3','EXIOBASE sector','sex','estimate','year'],axis=0).sum()
injuries_split_EXIO3_aggregate=injuries_split_EXIO3_aggregate.drop(columns='ISO3')
injuries_split_EXIO3_aggregate.to_csv("injuries_per_EXIO3.csv")

