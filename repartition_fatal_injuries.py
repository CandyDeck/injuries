import pandas as pd

fatal_injuries_europe_2008  = pd.read_csv("aux/data.csv")
fatal_injuries_europe_2008=fatal_injuries_europe_2008.drop(columns=['DATAFLOW','LAST UPDATE', 'freq', 'unit','OBS_FLAG'])
fatal_injuries_europe_2008=fatal_injuries_europe_2008[~fatal_injuries_europe_2008['nace_r2'].str.contains('A_C-N',regex=True)]


fatal_injuries_europe_2008_new = pd.DataFrame(columns = fatal_injuries_europe_2008.columns)

for year in fatal_injuries_europe_2008.TIME_PERIOD.unique():
    print(year)
    if (year<2010) :
        print ('inf 10')
        fatal_injuries_europe_2008_new = fatal_injuries_europe_2008_new.append(fatal_injuries_europe_2008[(fatal_injuries_europe_2008['TIME_PERIOD']==year) & (fatal_injuries_europe_2008['geo']=='EU27_2007')])
    
    else :
        if (year>=2019):
            print('plus 2018')
            fatal_injuries_europe_2008_new = fatal_injuries_europe_2008_new.append(fatal_injuries_europe_2008[(fatal_injuries_europe_2008['TIME_PERIOD']==year) & (fatal_injuries_europe_2008['geo']=='EU27_2020')])
        else :
            if (year>2009 & year<=2018):
                print('entre 10 et 18')
                #fatal_injuries_europe_2008_new = fatal_injuries_europe_2008_new.append(fatal_injuries_europe_2008[fatal_injuries_europe_2008['geo'].str.contains('EU28',regex=True)])
                fatal_injuries_europe_2008_new = fatal_injuries_europe_2008_new.append(fatal_injuries_europe_2008[(fatal_injuries_europe_2008['TIME_PERIOD']==year) & (fatal_injuries_europe_2008['geo']=='EU28')])

fatal_injuries_europe_2008_new["%"] = " "
fatal_injuries_europe_2008_new['TIME_PERIOD']=fatal_injuries_europe_2008_new['TIME_PERIOD'].astype(str).astype(int)

for row in fatal_injuries_europe_2008_new.index:
    year = fatal_injuries_europe_2008_new.loc[row,['TIME_PERIOD']].values[0]
    value = fatal_injuries_europe_2008_new.loc[row,['OBS_VALUE']].values[0]
    #print(fatal_injuries_europe_2008_new.loc[row,['TIME_PERIOD']==year])
    total = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='TOTAL') ,['OBS_VALUE']].to_string(header=None,index=None))
    fatal_injuries_europe_2008_new.loc[row,['%']] = 100*value/total
       

fatal_injuries_world = pd.read_csv("injuries_per_ISO3.csv")

for row in fatal_injuries_world.index:
    code = fatal_injuries_world.loc[row,['ISO3']].values[0]
    sector = fatal_injuries_world.loc[row,['EXIOBASE sector']].values[0]
    sex = fatal_injuries_world.loc[row,['sex']].values[0]
    estimate = fatal_injuries_world.loc[row,['estimate']].values[0]
    year =  fatal_injuries_world.loc[row,['year']].values[0]
    number = fatal_injuries_world.loc[row,['death']].values[0]
    if sector == 'A':
        print(code, year)
        a = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='A') ,['OBS_VALUE']].to_string(header=None,index=None))
        a1 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='A01') ,['OBS_VALUE']].to_string(header=None,index=None)) 
        a2 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='A02') ,['OBS_VALUE']].to_string(header=None,index=None)) 
        a3 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='A03') ,['OBS_VALUE']].to_string(header=None,index=None)) 

        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'A01',sex,estimate,year, (a1/a) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'A02',sex,estimate,year, (a2/a) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'A03',sex,estimate,year, (a3/a) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

    else :
        if sector == 'B':
            b = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B') ,['OBS_VALUE']].to_string(header=None,index=None))
            b5 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B05') ,['OBS_VALUE']].to_string(header=None,index=None)) 
            b6 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B06') ,['OBS_VALUE']].to_string(header=None,index=None)) 
            b7 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B07') ,['OBS_VALUE']].to_string(header=None,index=None)) 
            b8 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B08') ,['OBS_VALUE']].to_string(header=None,index=None)) 
            b9 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B09') ,['OBS_VALUE']].to_string(header=None,index=None)) 
    
            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'B05',sex,estimate,year, (b5/b) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'B06',sex,estimate,year, (b6/b) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'B07',sex,estimate,year, (b7/b) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'B08',sex,estimate,year, (b8/b) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'B09',sex,estimate,year, (b9/b) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
        
        else :
            if sector == 'C':
                c = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C') ,['OBS_VALUE']].to_string(header=None,index=None))
                c10 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C10') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c11 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C11') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c12 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C12') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c13 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C13') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c14 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C14') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c15 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C15') ,['OBS_VALUE']].to_string(header=None,index=None))
                c16 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C16') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c17 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C17') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c18 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C18') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c19 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C19') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c20 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C20') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c21 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C21') ,['OBS_VALUE']].to_string(header=None,index=None))
                c22 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C22') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c23 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C23') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c24 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C24') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c25 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C25') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c26 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C26') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c27 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C27') ,['OBS_VALUE']].to_string(header=None,index=None))
                c28 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C28') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c29 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C29') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c30 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C30') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c31 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C31') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c32 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C32') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c33 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C33') ,['OBS_VALUE']].to_string(header=None,index=None)) 

        
        
        
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C10',sex,estimate,year, (c10/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C11',sex,estimate,year, (c11/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C12',sex,estimate,year, (c12/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C13',sex,estimate,year, (c13/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C14',sex,estimate,year, (c14/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C15',sex,estimate,year, (c15/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C16',sex,estimate,year, (c16/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C17',sex,estimate,year, (c17/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C18',sex,estimate,year, (c18/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C19',sex,estimate,year, (c19/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C20',sex,estimate,year, (c20/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C21',sex,estimate,year, (c21/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C22',sex,estimate,year, (c22/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C23',sex,estimate,year, (c23/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C24',sex,estimate,year, (c24/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C25',sex,estimate,year, (c25/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C26',sex,estimate,year, (c26/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C27',sex,estimate,year, (c27/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C28',sex,estimate,year, (c28/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C29',sex,estimate,year, (c29/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C30',sex,estimate,year, (c30/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C31',sex,estimate,year, (c31/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C32',sex,estimate,year, (c32/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C33',sex,estimate,year, (c33/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)


            else :
                if sector == 'D':
                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'D35',sex,estimate,year, number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                else :
                    if sector == 'E':
                        e = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='E') ,['OBS_VALUE']].to_string(header=None,index=None))
                        e36 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='E36') ,['OBS_VALUE']].to_string(header=None,index=None))                         
                        e37 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='E37') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                        e38 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='E38') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                        e39 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='E39') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                
                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'E36',sex,estimate,year, (e36/e) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'E37',sex,estimate,year, (e37/e) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'E38',sex,estimate,year, (e38/e) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'E39',sex,estimate,year, (e39/e) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                    else :
                        if sector == 'F':
                            f = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='F') ,['OBS_VALUE']].to_string(header=None,index=None))
                            f41 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='F41') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                            f42 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='F42') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                            f43 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='F43') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                    
                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'F41',sex,estimate,year, (f41/f) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'F42',sex,estimate,year, (f42/f) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'F43',sex,estimate,year, (f43/f) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)


                        else :
                            if sector == 'G':
                                g = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='G') ,['OBS_VALUE']].to_string(header=None,index=None))
                                g45 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='G45') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                g46 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='G46') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                g47= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='G47') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                        
                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'G45',sex,estimate,year, (g45/g) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'G46',sex,estimate,year, (g46/g) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'G47',sex,estimate,year, (g47/g) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
 
                            else :
                                if sector == 'H':
                                    h = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H') ,['OBS_VALUE']].to_string(header=None,index=None))
                                    h49 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H49') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                    h50 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H50') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                    h51= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H51') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                    h52 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H52') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                    h53= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H53') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                
                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'H49',sex,estimate,year, (h49/h) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'H50',sex,estimate,year, (h50/h) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'H51',sex,estimate,year, (h51/h) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'H52',sex,estimate,year, (h52/h) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'H53',sex,estimate,year, (h53/h) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                                else :
                                    if sector == 'I':
                                        i = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='I') ,['OBS_VALUE']].to_string(header=None,index=None))
                                        i55 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='I55') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                        i56 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='I56') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                
                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'I55',sex,estimate,year, (i55/i) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'I56',sex,estimate,year, (i56/i) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                                    else :
                                        if sector == 'J':
                                            j = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J') ,['OBS_VALUE']].to_string(header=None,index=None))
                                            j58 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J58') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                            j59 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J59') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                            j60= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J60') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                            j61 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J61') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                            j62= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J62') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                            j63= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J63') ,['OBS_VALUE']].to_string(header=None,index=None)) 
        
                                            
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J58',sex,estimate,year, (j58/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J59',sex,estimate,year, (j59/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J60',sex,estimate,year, (j60/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J61',sex,estimate,year, (j61/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J62',sex,estimate,year, (j62/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J63',sex,estimate,year, (j63/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                                        else :
                                            if sector == 'K':
                                                k = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='K') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                k64 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='K64') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                k65 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='K65') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                k66 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='K66') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                        
                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'K64',sex,estimate,year, (k64/k) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'K65',sex,estimate,year, (k65/k) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'K66',sex,estimate,year, (k66/k) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                 
                                            else :
                                                if sector == 'L':
                                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'L68',sex,estimate,year, number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)


                                                else :
                                                    if sector == 'M':
                                                        m = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                        m69 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M69') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                        m70 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M70') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                        m71 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M71') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                        m72 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M72') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                        m73 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M73') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                        m74 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M74') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                        m75 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M75') ,['OBS_VALUE']].to_string(header=None,index=None)) 

                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M69',sex,estimate,year, (m69/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M70',sex,estimate,year, (m70/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M71',sex,estimate,year, (m71/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M72',sex,estimate,year, (m72/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M73',sex,estimate,year, (m73/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M74',sex,estimate,year, (m74/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M75',sex,estimate,year, (m75/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        
                                                    else :
                                                        if sector == 'N':
                                                            n = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                            n77 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N77') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                            n78 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N78') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                            n79= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N79') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                            n80 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N80') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                            n81= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N81') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                            n82= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N82') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                        
                                                            
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N77',sex,estimate,year, (n77/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N78',sex,estimate,year, (n78/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N79',sex,estimate,year, (n79/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N80',sex,estimate,year, (n80/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N81',sex,estimate,year, (n81/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N82',sex,estimate,year, (n82/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                                                        else :
                                                            if sector == 'O':
                                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'O84',sex,estimate,year, number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                                                            else :
                                                                if sector == 'P':
                                                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'P85',sex,estimate,year, number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                    
                                                                else :
                                                                    if sector == 'Q':
                                                                        q = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='Q') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                                        q86 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='Q86') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                        q87 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='Q87') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                        q88 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='Q88') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                
                                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'Q86',sex,estimate,year, (q86/q) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'Q87',sex,estimate,year, (q87/q) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'Q88',sex,estimate,year, (q88/q) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                        
                                                                        
                                                                    else :
                                                                        if sector == 'R':
                                                                            r = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='R') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                                            r90 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='R90') ,['OBS_VALUE']].to_string(header=None,index=None))                         
                                                                            r91 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='R91') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                            r92 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='R92') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                            r93 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='R93') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                    
                                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'R90',sex,estimate,year, (r90/r) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'R91',sex,estimate,year, (r91/r) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'R92',sex,estimate,year, (r92/r) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'R93',sex,estimate,year, (r93/r) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                    
                                                                        else :
                                                                            if sector == 'S':
                                                                                s = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='S') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                                                s94 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='S94') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                                s95 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='S95') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                                s96 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='S96') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                        
                                                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'S94',sex,estimate,year, (s94/s) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'S95',sex,estimate,year, (s95/s) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'S96',sex,estimate,year, (s96/s) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)


                                                                            else :
                                                                                if sector == 'T':
                                                                                    t = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='T') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                                                    t97 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='T97') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                                    t98 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='T98') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                            
                                                                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'T97',sex,estimate,year, (t97/t) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'T98',sex,estimate,year, (t98/t) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                                                 
                                                                                else :
                                                                                    if sector == 'U':
                                                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'U99',sex,estimate,year, number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                    
                                                    
fatal_injuries_world_ISO3 = fatal_injuries_world.copy()
fatal_injuries_world_ISO3=fatal_injuries_world_ISO3.round(0)
fatal_injuries_world_ISO3.to_csv('fatal_injuries_world_ISO3.csv',index=False)

fatal = {}
writer = pd.ExcelWriter('fatal_injuries_ISO3.xlsx',engine='xlsxwriter')
for year in fatal_injuries_world_ISO3['year'].unique():
    fatal[year]=fatal_injuries_world_ISO3[fatal_injuries_world_ISO3['year']==year]
    fatal[year].to_excel(writer, sheet_name=str(year),index=False)
writer.save()

fatal_injuries_world = pd.read_csv("injuries_per_EXIO3.csv")


for row in fatal_injuries_world.index:
    code = fatal_injuries_world.loc[row,['EXIO3']].values[0]
    sector = fatal_injuries_world.loc[row,['EXIOBASE sector']].values[0]
    sex = fatal_injuries_world.loc[row,['sex']].values[0]
    estimate = fatal_injuries_world.loc[row,['estimate']].values[0]
    year =  fatal_injuries_world.loc[row,['year']].values[0]
    number = fatal_injuries_world.loc[row,['death']].values[0]
    if sector == 'A':
        print(code, year)
        a = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='A') ,['OBS_VALUE']].to_string(header=None,index=None))
        a1 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='A01') ,['OBS_VALUE']].to_string(header=None,index=None)) 
        a2 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='A02') ,['OBS_VALUE']].to_string(header=None,index=None)) 
        a3 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='A03') ,['OBS_VALUE']].to_string(header=None,index=None)) 

        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'A01',sex,estimate,year, (a1/a) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'A02',sex,estimate,year, (a2/a) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'A03',sex,estimate,year, (a3/a) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

    else :
        if sector == 'B':
            b = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B') ,['OBS_VALUE']].to_string(header=None,index=None))
            b5 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B05') ,['OBS_VALUE']].to_string(header=None,index=None)) 
            b6 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B06') ,['OBS_VALUE']].to_string(header=None,index=None)) 
            b7 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B07') ,['OBS_VALUE']].to_string(header=None,index=None)) 
            b8 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B08') ,['OBS_VALUE']].to_string(header=None,index=None)) 
            b9 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='B09') ,['OBS_VALUE']].to_string(header=None,index=None)) 
    
            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'B05',sex,estimate,year, (b5/b) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'B06',sex,estimate,year, (b6/b) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'B07',sex,estimate,year, (b7/b) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'B08',sex,estimate,year, (b8/b) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'B09',sex,estimate,year, (b9/b) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
        
        else :
            if sector == 'C':
                c = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C') ,['OBS_VALUE']].to_string(header=None,index=None))
                c10 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C10') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c11 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C11') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c12 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C12') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c13 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C13') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c14 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C14') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c15 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C15') ,['OBS_VALUE']].to_string(header=None,index=None))
                c16 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C16') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c17 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C17') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c18 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C18') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c19 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C19') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c20 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C20') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c21 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C21') ,['OBS_VALUE']].to_string(header=None,index=None))
                c22 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C22') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c23 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C23') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c24 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C24') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c25 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C25') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c26 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C26') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c27 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C27') ,['OBS_VALUE']].to_string(header=None,index=None))
                c28 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C28') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c29 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C29') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c30 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C30') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c31 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C31') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c32 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C32') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                c33 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='C33') ,['OBS_VALUE']].to_string(header=None,index=None)) 

        
        
        
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C10',sex,estimate,year, (c10/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C11',sex,estimate,year, (c11/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C12',sex,estimate,year, (c12/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C13',sex,estimate,year, (c13/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C14',sex,estimate,year, (c14/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C15',sex,estimate,year, (c15/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C16',sex,estimate,year, (c16/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C17',sex,estimate,year, (c17/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C18',sex,estimate,year, (c18/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C19',sex,estimate,year, (c19/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C20',sex,estimate,year, (c20/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C21',sex,estimate,year, (c21/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C22',sex,estimate,year, (c22/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C23',sex,estimate,year, (c23/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C24',sex,estimate,year, (c24/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C25',sex,estimate,year, (c25/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C26',sex,estimate,year, (c26/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C27',sex,estimate,year, (c27/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C28',sex,estimate,year, (c28/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C29',sex,estimate,year, (c29/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C30',sex,estimate,year, (c30/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C31',sex,estimate,year, (c31/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C32',sex,estimate,year, (c32/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'C33',sex,estimate,year, (c33/c) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)


            else :
                if sector == 'D':
                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'D35',sex,estimate,year, number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                else :
                    if sector == 'E':
                        e = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='E') ,['OBS_VALUE']].to_string(header=None,index=None))
                        e36 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='E36') ,['OBS_VALUE']].to_string(header=None,index=None))                         
                        e37 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='E37') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                        e38 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='E38') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                        e39 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='E39') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                
                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'E36',sex,estimate,year, (e36/e) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'E37',sex,estimate,year, (e37/e) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'E38',sex,estimate,year, (e38/e) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'E39',sex,estimate,year, (e39/e) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                    else :
                        if sector == 'F':
                            f = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='F') ,['OBS_VALUE']].to_string(header=None,index=None))
                            f41 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='F41') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                            f42 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='F42') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                            f43 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='F43') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                    
                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'F41',sex,estimate,year, (f41/f) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'F42',sex,estimate,year, (f42/f) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'F43',sex,estimate,year, (f43/f) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)


                        else :
                            if sector == 'G':
                                g = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='G') ,['OBS_VALUE']].to_string(header=None,index=None))
                                g45 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='G45') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                g46 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='G46') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                g47= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='G47') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                        
                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'G45',sex,estimate,year, (g45/g) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'G46',sex,estimate,year, (g46/g) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'G47',sex,estimate,year, (g47/g) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
 
                            else :
                                if sector == 'H':
                                    h = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H') ,['OBS_VALUE']].to_string(header=None,index=None))
                                    h49 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H49') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                    h50 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H50') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                    h51= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H51') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                    h52 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H52') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                    h53= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='H53') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                
                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'H49',sex,estimate,year, (h49/h) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'H50',sex,estimate,year, (h50/h) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'H51',sex,estimate,year, (h51/h) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'H52',sex,estimate,year, (h52/h) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'H53',sex,estimate,year, (h53/h) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                                else :
                                    if sector == 'I':
                                        i = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='I') ,['OBS_VALUE']].to_string(header=None,index=None))
                                        i55 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='I55') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                        i56 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='I56') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                
                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'I55',sex,estimate,year, (i55/i) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'I56',sex,estimate,year, (i56/i) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                                    else :
                                        if sector == 'J':
                                            j = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J') ,['OBS_VALUE']].to_string(header=None,index=None))
                                            j58 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J58') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                            j59 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J59') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                            j60= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J60') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                            j61 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J61') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                            j62= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J62') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                            j63= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='J63') ,['OBS_VALUE']].to_string(header=None,index=None)) 
        
                                            
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J58',sex,estimate,year, (j58/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J59',sex,estimate,year, (j59/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J60',sex,estimate,year, (j60/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J61',sex,estimate,year, (j61/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J62',sex,estimate,year, (j62/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'J63',sex,estimate,year, (j63/j) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                                        else :
                                            if sector == 'K':
                                                k = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='K') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                k64 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='K64') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                k65 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='K65') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                k66 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='K66') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                        
                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'K64',sex,estimate,year, (k64/k) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'K65',sex,estimate,year, (k65/k) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'K66',sex,estimate,year, (k66/k) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                 
                                            else :
                                                if sector == 'L':
                                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'L68',sex,estimate,year, number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)


                                                else :
                                                    if sector == 'M':
                                                        m = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                        m69 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M69') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                        m70 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M70') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                        m71 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M71') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                        m72 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M72') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                        m73 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M73') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                        m74 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M74') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                        m75 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='M75') ,['OBS_VALUE']].to_string(header=None,index=None)) 

                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M69',sex,estimate,year, (m69/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M70',sex,estimate,year, (m70/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M71',sex,estimate,year, (m71/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M72',sex,estimate,year, (m72/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M73',sex,estimate,year, (m73/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M74',sex,estimate,year, (m74/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'M75',sex,estimate,year, (m75/m) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                        
                                                    else :
                                                        if sector == 'N':
                                                            n = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                            n77 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N77') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                            n78 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N78') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                            n79= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N79') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                            n80 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N80') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                            n81= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N81') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                            n82= float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='N82') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                        
                                                            
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N77',sex,estimate,year, (n77/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N78',sex,estimate,year, (n78/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N79',sex,estimate,year, (n79/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N80',sex,estimate,year, (n80/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N81',sex,estimate,year, (n81/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'N82',sex,estimate,year, (n82/n) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                                                        else :
                                                            if sector == 'O':
                                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'O84',sex,estimate,year, number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)

                                                            else :
                                                                if sector == 'P':
                                                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'P85',sex,estimate,year, number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                    
                                                                else :
                                                                    if sector == 'Q':
                                                                        q = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='Q') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                                        q86 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='Q86') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                        q87 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='Q87') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                        q88 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='Q88') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                
                                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'Q86',sex,estimate,year, (q86/q) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'Q87',sex,estimate,year, (q87/q) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'Q88',sex,estimate,year, (q88/q) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                        
                                                                        
                                                                    else :
                                                                        if sector == 'R':
                                                                            r = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='R') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                                            r90 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='R90') ,['OBS_VALUE']].to_string(header=None,index=None))                         
                                                                            r91 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='R91') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                            r92 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='R92') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                            r93 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='R93') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                    
                                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'R90',sex,estimate,year, (r90/r) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'R91',sex,estimate,year, (r91/r) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'R92',sex,estimate,year, (r92/r) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                            fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'R93',sex,estimate,year, (r93/r) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                    
                                                                        else :
                                                                            if sector == 'S':
                                                                                s = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='S') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                                                s94 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='S94') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                                s95 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='S95') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                                s96 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='S96') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                        
                                                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'S94',sex,estimate,year, (s94/s) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'S95',sex,estimate,year, (s95/s) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                                fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'S96',sex,estimate,year, (s96/s) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)


                                                                            else :
                                                                                if sector == 'T':
                                                                                    t = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='T') ,['OBS_VALUE']].to_string(header=None,index=None))
                                                                                    t97 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='T97') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                                    t98 = float(fatal_injuries_europe_2008_new.loc[(fatal_injuries_europe_2008_new['TIME_PERIOD']==year)&(fatal_injuries_europe_2008_new['nace_r2']=='T98') ,['OBS_VALUE']].to_string(header=None,index=None)) 
                                                                            
                                                                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'T97',sex,estimate,year, (t97/t) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                                    fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'T98',sex,estimate,year, (t98/t) * number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                                                 
                                                                                else :
                                                                                    if sector == 'U':
                                                                                        fatal_injuries_world=fatal_injuries_world.append(pd.Series([code,'U99',sex,estimate,year, number],index=[i for i in fatal_injuries_world.columns]),ignore_index=True)
                                                                    
                                                    
fatal_injuries_world_EXIO3 = fatal_injuries_world.copy()
fatal_injuries_world_EXIO3=fatal_injuries_world_EXIO3.round(0)
fatal_injuries_world_EXIO3.to_csv('fatal_injuries_world_EXIO3.csv',index=False)

fatal = {}
writer = pd.ExcelWriter('fatal_injuries_EXIO3.xlsx',engine='xlsxwriter')
for year in fatal_injuries_world_EXIO3['year'].unique():
    fatal[year]=fatal_injuries_world_EXIO3[fatal_injuries_world_EXIO3['year']==year]
    fatal[year].to_excel(writer, sheet_name=str(year),index=False)
writer.save()


